package com.eliptico.cls.gsti_cls;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by manikantad on 28-02-2017.
 */

public class GSTIApplication extends Application {

    private static Application instance;

    @Override
    public void onCreate() {
        super.onCreate();



        CalligraphyConfig calligraphyConfig = new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build();

        CalligraphyConfig.initDefault(calligraphyConfig);

        instance = this;
    }



    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static ProgressDialog showProgressDialog(Context context, String text){
        ProgressDialog m_Dialog = new ProgressDialog(context);
        m_Dialog.setMessage(text);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();
        return m_Dialog;
    }



}