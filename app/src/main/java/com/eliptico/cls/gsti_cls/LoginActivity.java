package com.eliptico.cls.gsti_cls;

import android.app.Activity;
import android.app.ProgressDialog;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.eliptico.model.LoginModel;
import com.eliptico.model.LoginResult;
import com.eliptico.rest.ApiInterface;
import com.eliptico.rest.ApiUtils;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity {

    private ApiInterface apiInterface;
    private EditText etxtPhone,etxtPassword;
    private ProgressDialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        etxtPhone = (EditText) findViewById(R.id.etxtphonenumber);
        etxtPassword = (EditText) findViewById(R.id.etxtpassword);

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void next(View v) {

        myDialog = GSTIApplication.showProgressDialog(this, getResources().getString(R.string.loading));

        apiInterface = ApiUtils.getAPIService();

        String phoneNumber = etxtPhone.getText().toString();
        String passWord = etxtPassword.getText().toString();

        if (phoneNumber.equalsIgnoreCase("") || passWord.equalsIgnoreCase("")) {
            myDialog.dismiss();
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.action_emptyfields), Toast.LENGTH_SHORT).show();
        } else {

            if (phoneNumber.contains(" ") || passWord.contains(" ")) {

                phoneNumber.trim().replace(" ", "");
                passWord.trim().replace(" ", "");
            }

            apiInterface.Login(phoneNumber, passWord).enqueue(new Callback<LoginModel>() {

                @Override
                public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                    if (response.code() == 200) {
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                        LoginResult result = response.body().getUser();
                        String firstName = result.getFirstName();
                        myDialog.dismiss();
                        Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("LoginResult", result);
                        i.putExtras(bundle);
                        startActivity(i);
                        finish();
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(LoginActivity.this, jObjError.getString("error"), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {

                        }

                    }
                    myDialog.dismiss();
                }

                @Override
                public void onFailure(Call<LoginModel> call, Throwable t) {
                    myDialog.dismiss();
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_failed), Toast.LENGTH_SHORT).show();
                    System.out.println("error " + t.toString());
                }
            });

        }


    }

}

