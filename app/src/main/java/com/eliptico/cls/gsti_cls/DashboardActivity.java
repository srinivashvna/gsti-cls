package com.eliptico.cls.gsti_cls;


import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ListPopupWindow;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.TextView;


import com.eliptico.adapter.ListPopupWindowAdapter;
import com.eliptico.model.LoginResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DashboardActivity extends AppCompatActivity {

    private ImageButton imgSetting,imgBack;
    private TextView txtDriverName,txtDriverTime,txtDriverRoute;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.custom_action_bar_layout);
        View view =getSupportActionBar().getCustomView();

        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        LoginResult result = (LoginResult) bundle.getSerializable("LoginResult");

        txtDriverName = (TextView)findViewById(R.id.driver_name);
        txtDriverTime = (TextView)findViewById(R.id.datetime);
        txtDriverRoute = (TextView)findViewById(R.id.route);


        SpannableString spannableDriverName = new SpannableString(txtDriverName.getText().toString()+"\n"+result.getFirstName());
        String totalNameLength = txtDriverName.getText().toString()+result.getFirstName();
        spannableDriverName.setSpan(new RelativeSizeSpan(2f),6,totalNameLength.length()+1,0);
        spannableDriverName.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(getResources(), R.color.blue, null)),6,totalNameLength.length()+1,0);
        txtDriverName.setText(spannableDriverName);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM, yyyy");
        String formattedDate = dateFormat.format(c.getTime());

        SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
        String formattedTime = timeFormat.format(c.getTime());

        SpannableString spannableTimeString = new SpannableString(formattedDate+"\n"+formattedTime);
        spannableTimeString.setSpan(new RelativeSizeSpan(2f),0,12,0);
        txtDriverTime.setText(spannableTimeString);





        imgSetting = (ImageButton)view.findViewById(R.id.action_bar_forward);
        imgSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onListPopUp(v);
            }
        });

        imgBack= (ImageButton)view.findViewById(R.id.action_bar_back);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public void onListPopUp(View anchor)
    {
        // This a sample dat to fill our ListView
        ArrayList<String> personItem = new ArrayList<String>();
        personItem.add(getResources().getString(R.string.about));
        personItem.add(getResources().getString(R.string.log_out));

        // Initialise our adapter
        ListPopupWindowAdapter mListPopUpAdapter = new ListPopupWindowAdapter(this, personItem);

        //Initialise our ListPopupWindow instance
        final ListPopupWindow pop = new ListPopupWindow(this);
        // Configure ListPopupWindow properties
        pop.setAdapter(mListPopUpAdapter);
        // Set the view below/above which ListPopupWindow dropdowns
        pop.setAnchorView(anchor);
        // Setting this enables window to be dismissed by click outside ListPopupWindow
        pop.setModal(true);
        // Sets the width of the ListPopupWindow
        pop.setContentWidth(350);
        // Sets the Height of the ListPopupWindow
        pop.setHeight(ListPopupWindow.WRAP_CONTENT);
        // Set up a click listener for the ListView items
        pop.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // Dismiss the LisPopupWindow when a list item is clicked
                pop.dismiss();
            }
        });
        pop.show();
    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }*/

}



