package com.eliptico.model;

import java.io.Serializable;

/**
 * Created by manikantad on 22-02-2017.
 */


public class LoginResult implements Serializable{

    private String teamName;
    private String lastName;
    private String organizationName;
    private String role;
    private String firstName;
    private String defaultRoute;



    public String getDefaultRoute() {
        return defaultRoute;
    }

    public void setDefaultRoute(String defaultRoute) {
        this.defaultRoute = defaultRoute;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "LoginResult [teamName = " + teamName + ", lastName = " + lastName + ", organizationName = " + organizationName + ", role = " + role + ", firstName = " + firstName + "]";
    }
}
