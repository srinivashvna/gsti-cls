package com.eliptico.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by manikantad on 22-02-2017.
 */

public class LoginModel {

    private String apiToken;
    private String refreshToken;
    private LoginResult user;

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public LoginResult getUser() {
        return user;
    }

    public void setUser(LoginResult user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "ClassPojo [apiToken = " + apiToken + ", refreshToken = " + refreshToken + ", user = " + user + "]";
    }
}