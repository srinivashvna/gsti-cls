package com.eliptico.rest;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by manikantad on 22-02-2017.
 */

public class ApiClient  {

    private static Retrofit retrofit = null;

    private static OkHttpClient getClient() {

        AnalyticsInterceptor analyticsInterceptor = new AnalyticsInterceptor();

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(analyticsInterceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();

        return client;
    }

    public static Retrofit getClient(String baseUrl) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getClient())
                    .build();
        }
        return retrofit;
    }
}