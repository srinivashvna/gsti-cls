package com.eliptico.rest;

/**
 * Created by manikantad on 23-02-2017.
 */

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "http://10.20.11.20:8026/";

    public static ApiInterface getAPIService() {

        return ApiClient.getClient(BASE_URL).create(ApiInterface.class);
    }
}
