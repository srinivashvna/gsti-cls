package com.eliptico.rest;


import android.content.Context;
import android.provider.Settings.Secure;

import com.eliptico.cls.gsti_cls.GSTIApplication;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by manikantad on 24-02-2017.
 */

 public  final class AnalyticsInterceptor implements Interceptor {

    Context context = GSTIApplication.getContext();

    private String android_id = Secure.getString(context.getContentResolver(),
            Secure.ANDROID_ID);

    @Override
    public Response intercept(Chain chain)
            throws IOException {
        Request request = chain.request();
        request = request.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", android_id)
                .build();
        Response response = chain.proceed(request);
        return response;
    }
}