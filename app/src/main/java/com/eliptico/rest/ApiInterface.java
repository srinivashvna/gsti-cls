package com.eliptico.rest;

import com.eliptico.model.LoginModel;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by manikantad on 22-02-2017.
 */

public interface ApiInterface {

    @POST("api/authentication/login")
    @FormUrlEncoded
    Call<LoginModel> Login(@Field("userName") String first, @Field("password") String last);

}

