package com.eliptico.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.eliptico.cls.gsti_cls.R;

import java.util.ArrayList;

/**
 * Created by manikantad on 23-02-2017.
 */

public class ListPopupWindowAdapter extends BaseAdapter {


    private Context context;
    private ArrayList<String> personItem;

    public ListPopupWindowAdapter(Context context, ArrayList<String> personItem) {
        this.context = context;
        this.personItem = personItem;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        TextView name;
        TextView userName;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        convertView = inflater.inflate(R.layout.toolbar_overfow_row_item_text, parent, false);


        name = (TextView) convertView.findViewById(R.id.textViewName);
        name.setText(personItem.get(position));


        return convertView;
    }

    @Override
    public Object getItem(int index) {
        return personItem.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return personItem.size();
    }

}